- Install EKS:
    cd eks && terraform init && terraform apply -auto-approve

- Install all component:
    cd helm && bash script.sh

- Install application:
    cd helm && kubectl apply -f deployment.yaml

- Benchmark:
    kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://http-service; done"

- Kibana access:
    URL: https://kibana.devopsquiz.com
    Choose "Explore my own data"
    Choose "Discover" on left menu
    Click "Create index patten"
    Enter "logstash-*" and click "Next step"
    Choose "@timestamp" at "Time Filter field name" and click "Create index pattern"
    Enter the URL: https://kibana.devopsquiz.com/app/kibana#/discover?_g=()

- Get Grafana admin password:
    kubectl get secret --namespace platform grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

- Grafana access:
    Enter the URL: https://kibana.devopsquiz.com/
    Login with admin user and password above

- Import Grafana dashboard:
    Click "+" button on left menu and choose "Import"
    Choose "Upload JSON file" and browse to file in `helm/