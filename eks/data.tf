data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "demo-nguyen-tfstate"
    key    = "vpc"
    region = "ap-southeast-1"
  }
}

data "aws_caller_identity" "current" {
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}
