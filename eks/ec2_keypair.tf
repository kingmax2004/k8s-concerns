resource "random_pet" "this" {
  length = 2
}

resource "tls_private_key" "this" {
  algorithm = "RSA"
}

resource "aws_key_pair" "this" {
  key_name   = random_pet.this.id
  public_key = tls_private_key.this.public_key_openssh
}