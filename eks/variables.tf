variable "region" {
  default = "ap-southeast-1"
}

variable "instance_type" {
  # Smallest recommended, where ~1.1Gb of 2Gb memory is available for the Kubernetes pods after ‘warming up’ Docker, Kubelet, and OS
  default = "t3a.small"
  type    = string
}

variable "kms_key_arn" {
  default     = ""
  description = "KMS key ARN to use if you want to encrypt EKS node root volumes"
  type        = string
}

variable "enable_cluster_autoscaler" {
  description = "Control if install Cluster Autoscaler or not"
  type        = bool
  default     = true
}

variable "config_output_path" {
  description = "Where to save the config files. Assumed to be a directory if the value ends with a forward slash `/`."
  type        = string
  default     = "./"
}

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  default     = []
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  default     = []
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  default     = []
}

variable "flink_node_groups" {
  description = "Configuration for Flink-dedicated node groups."
  default = {
    # Each node group is in a single AZ.
    zones = ["a", "b", "c"]
    # The total number of node groups is `len(zones) * len(sizes)`.
    sizes = {
      xlarge = {
        max_capacity = 64
        disk_size = 50
      }
      "2xlarge" = {
        max_capacity = 32
        disk_size = 100
      }
      "4xlarge" = {
        max_capacity = 16
        disk_size = 200
      }
    }
  }
}
