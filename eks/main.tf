provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}

module "eks" {
  source                         = "terraform-aws-modules/eks/aws"
  cluster_name                   = local.cluster_name
  cluster_version                = "1.19"
  vpc_id                         = data.terraform_remote_state.vpc.outputs.vpc_id
  subnets                        = data.terraform_remote_state.vpc.outputs.private_subnets
  cluster_endpoint_public_access = true
  enable_irsa                    = true
  write_kubeconfig               = false

  cluster_endpoint_private_access                = true
  cluster_create_endpoint_private_access_sg_rule = true

  cluster_endpoint_private_access_cidrs = [
    "10.16.0.0/16"
  ]

  node_groups_defaults = {
    k8s_labels = {
      environment = "production"
      cluster     = "production"
    }
  }

  # TODO: Reduce the amount of boilerplate code here.
  node_groups = merge({
    on_demand = {
      node_group_name_prefix = "on_demand"

      desired_capacity = 2
      max_capacity     = 2
      min_capacity     = 1

      capacity_type  = "ON_DEMAND"
      instance_types = ["m4.large"]

      launch_template_id      = aws_launch_template.default.id
      launch_template_version = aws_launch_template.default.default_version

      k8s_labels = {
        nodegroup = "on_demand"
      }
    }
  })

  map_accounts = ["592444560930"]

  map_users = [
    {
      userarn  = "" //iam user
      username = ""
      groups   = ["system:masters"]
    }
  ]
  map_roles = []
}
