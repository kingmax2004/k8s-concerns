terraform {
  required_providers {

    aws = {
      version = "~> 3.47.0"
      source  = "hashicorp/aws"
    }

    random = {
      version = "~> 3.1.0"
    }

    local = {
      version = "~> 2.1.0"
    }

    null = {
      version = "~> 3.1.0"
    }

    template = {
      version = "~> 2.2.0"
    }

    kubernetes = {
      version = ">= 2.0.2"
    }

    tls = {
      version = ">= 1.0"
    }
  }
  required_version = "~> 0.14"
}

provider "aws" {
  region = "ap-southeast-1"
}
