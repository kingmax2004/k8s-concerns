helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add stable https://charts.helm.sh/stable
helm repo update
kubectl create ns platform
helm upgrade -i ingress-nginx ingress-nginx/ingress-nginx
helm upgrade -i prometheus -n platform prometheus-community/prometheus
helm upgrade -i external-dns -n platform bitnami/external-dns -f external-dns.yaml
helm upgrade -i elasticsearch -n platform stable/elasticsearch -f elasticsearch.yaml
helm upgrade -i fluentd -n platform stable/fluentd-elasticsearch
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission
helm upgrade -i kibana -n platform stable/kibana  -f kibana.yaml
helm upgrade -i grafana -n platform stable/grafana -f grafana.yaml
