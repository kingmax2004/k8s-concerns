module "vpc" {

  source = "git::ssh://git@git.parcelperform.com/devops/terraform-aws-modules.git//vpc?ref=v1.0.2"

  name = "production"

  cidr = "10.20.0.0/16"

  azs             = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
  private_subnets = ["10.20.0.0/19", "10.20.32.0/19", "10.20.64.0/19"]
  public_subnets  = ["10.20.128.0/20", "10.20.144.0/20", "10.20.160.0/20"]

  enable_ipv6 = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_dhcp_options      = true
  dhcp_options_domain_name = "ap-southeast-1.compute.internal"

  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false

  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = 1
  }

  public_subnet_tags = {
    "kubernetes.io/role/elb" = 1
  }
}